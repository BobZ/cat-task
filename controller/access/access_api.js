const db_pool = require('../../db/db_pool.js');
let access_api = {}


access_api.login = async function(username,password){
    const promisePool = db_pool.promise();
    let [rows,fields] = await promisePool.query("SELECT * FROM `admin` WHERE `username` = ? AND `password` = ?",
        [username,password]
    );
    if(rows.length!=1)
    {
        return false;
    }
    return true;
}
module.exports = access_api;