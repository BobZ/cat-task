const client_api = {};


//获取所有客户端uuid
client_api.get_client_all = function(){
    //获取客户端的uuid 
    let clientData = {};
    clientData.clist = [];
    for(let client in global.cserver.clients)
    {
        let c = global.cserver.clients[client]
        clientData.clist.push(c.uuid);
    }
    clientData.count = clientData.clist.length;
    return clientData;
}

//给指定的客户端发送数据
client_api.send_msg = function(client_uuid,msg){
    let client = global.cserver.clients[client_uuid];
    if(client != undefined)
    {
        try {
            global.cserver.clients[client_uuid].write(msg);
        } catch (error) {
            console.warn("给cs客户端 %s 发送消息失败",client_uuid);
        }
    }
    else
    {
        console.warn("cs客户端 %s 不存在",client_uuid);
    }
}


module.exports = client_api