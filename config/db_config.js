const fs = require('fs');
const yaml = require('js-yaml');
let db_config = {
    host: "localhost",
    user: "cat",
    password: "zhiyuan",
    port: 3306,
    database: "cattask",
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
};
try {
    let db_config_yaml = fs.readFileSync(`${__dirname}/db_config.yaml`);
    db_config = yaml.load(db_config_yaml);
} catch (error) {
    console.warn("db_config.yaml不存在,正在初始化创建!");
    db_config_yaml = yaml.dump(db_config);
    fs.writeFileSync(`${__dirname}/db_config.yaml`, db_config_yaml);
    
}
module.exports = db_config