const fs = require('fs');
const yaml = require('js-yaml');
let server_config = {
    bsport: 2021,
    csport: 2022,
    limit: "50mb",
};
try {
    let server_config_yaml = fs.readFileSync(`${__dirname}/server_config.yaml`, 'utf8');
    server_config = yaml.load(server_config_yaml);
} catch (error) {
    console.warn("server_config.yaml不存在,正在初始化创建!");
    let server_config_yaml = yaml.dump(server_config);
    fs.writeFileSync(`${__dirname}/server_config.yaml`, server_config_yaml, 'utf8');
    
}
module.exports = server_config