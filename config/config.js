const db_config = require('./db_config');
const server_config = require('./server_config');
module.exports = {
    db_config: db_config,
    server_config: server_config
}