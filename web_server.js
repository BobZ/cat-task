const config = require('./config/config')
const express = require('express')
const bodyParser = require('body-parser');
const path = require('path')
const app = express()
const session = require("express-session");
app.use(bodyParser.json());
app.use(session({
    secret:"CatTask Cat",
    resave:false,
    saveUninitialized:true
}))
//引用路由
const { index } = require("./route/index");
const { admin } = require("./route/admin");
const { client } = require("./route/client");
const { doc } = require("./route/doc");
// 静态资源
app.use(express.static(path.join(__dirname, "public")));
// 防止数据过大
app.use(express.json({ limit: config.server_config.limit }));
app.use(express.urlencoded({ limit: config.server_config.limit, extended: true }));
// 获取post参数
app.use(express.urlencoded({ extended: false }));
// 使用 engine 方法向 express 声明使用的模板引擎和模板文件后缀名
app.engine('art', require('express-art-template'))
// 使用 set 方法告诉 express 模板文件的路径
app.set('views', path.join(__dirname, 'view'))
// 告诉 express 默认模板文件的后缀名
app.set('view engine', 'art')
// 分配路由
app.use("/", index);
app.use('/admin', admin);
app.use('/client', client);
app.use('/doc', doc);

app.use((req, res) => {
    res.send("404")
})
//启动web管理服务
app.listen(config.server_config.bsport, () => 
    console.log(`Bs管理页面启动成功  http://127.0.0.1:%s/`,config.server_config.bsport)
)
module.exports = app