const config = require('./config/config')
const net = require('net');
const msgstick = require('stickpackage');
const { v4: uuidv4 } = require('uuid');

const app = net.createServer();
app.clients = {};
app.on('connection',(client)=>{
    client.setEncoding('hex');
    client.uuid = uuidv4();
    //匹配小端模式
    client.msg = new msgstick.msgCenter({bigEndian:false,type:32,bufferSize:1024});
    // 记录链接的进程
    app.clients[client.uuid] = client;
    // 客户socket进程绑定事件
    client.on('data',function(chunk){
        //压入缓冲区 处理粘包 分包
        client.msg.putData(Buffer.from(chunk,"hex"));
    })
    client.msg.onMsgRecv((data) => {
        //处理完粘包的数据
        console.log(data.length);//,data.toString()
    });
    client.on('close',(p1)=>{
        delete app.clients[client.uuid]
    })

    client.on('error',(p1)=>{
        client.end();
    })
})

app.listen(config.server_config.csport,()=>{
    console.log(`Cs服务器启动成功  端口:`,config.server_config.csport)
});

module.exports = app;