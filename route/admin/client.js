const express = require('express')
const client = express.Router()



client.get('/list', (_req, res) => {
    if(_req.session.userinfo){
        res.render('admin/client_list')
    }
    else{
        res.render('admin/login')
    }
});
module.exports = {
    client
}