const express = require('express')
const access_api = require('@/controller/access/access_api')
const Validate = require('request-validate')
const login = express.Router()

// 登录请求
login.post('/', (_req, res) => {
  /* 
  required: 必填字段
  numeric： 必须为数字
  array： 必须是数组
  min： 数字：最小值，字符：最小长度
  max：
  in：必须在范围内 eg: in:a,b,c
  */
  let ret = {code:500,msg:"",session:""}
  let inputData = _req.body;
  let rules = {
    'username': 'required|min:6',
    'password': 'required|min:6',
  }
  let message = {
    'username.required': '用户名不能为空',
    'username.min': '用户名不能少于6位',
    'password.required': '密码不能为空',
    'password.min': '密码不能少于6位'
  }
  try {
    //校验参数
    Validate(inputData, rules, message);
    //校验账号密码
    access_api.login(inputData.username,inputData.password).then((x)=>{
      if(x){
        //开始生成 session
        _req.session.userinfo = inputData.username;
        ret.code = 200;
        ret.msg = "登录成功";
        res.send(ret);
      }
      else{
        ret.msg = "账号或密码错误";
        res.send(ret);
      }
    })
  } catch (error) {
    ret.msg = error.message;
    res.send(ret);
  }
  
});

// get方式请求login接口，渲染登录页面
login.get('/', (_req, res) => {
  if(_req.session.userinfo){
    res.redirect("/admin");
  }
  else{
    res.render('admin/login')
  }
})

module.exports = {
    login
}