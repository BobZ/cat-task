const express = require('express')
const index = express.Router()



index.get('/', (_req, res) => {
    if(_req.session.userinfo){
        res.render('admin/index')
    }
    else{
        res.render('admin/login')
    }
});
module.exports = {
    index
}