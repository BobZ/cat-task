const express = require('express')
const { login } = require('./admin/login')
const { index } = require('./admin/index')
const { client } = require('./admin/client')
const admin = express.Router()
admin.use('/login', login)
admin.use('/index', index)
admin.use('/client', client)

admin.get('/', (_req, res) => {
    if(_req.session.userinfo){
        res.render('admin')
      }
      else{
        res.redirect("admin/login");
      }
      
})
admin.use((req, res) => {
    res.send("404")
})
module.exports = {admin}