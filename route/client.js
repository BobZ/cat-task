const express = require('express')
const client_api = require('@/controller/client/client_api')
const atob = require("atob")
const client = express.Router()


client.get('/',(req, res) => {
    let ret = {code:500,msg:"",data:{}}
    //检查session
    if(req.session.userinfo){
        ret.code = 200;
        ret.msg = "成功";
        ret.data = client_api.get_client_all();
        res.json(ret);
    }
    else{
        ret.msg = "未授权";

        res.json(ret);
    }
});

//cuuid  msg
client.post('/send',(req, res) => {
    let ret = {code:500,msg:""}
    if(!req.session.userinfo){
        ret.msg = "未授权";
        res.json(ret);
        return;
    }
    //检查session
    try {
        client_api.send_msg(req.body.cuuid,Buffer.from(req.body.msg, 'base64'));
        ret.code = 200;
        ret.msg = "成功";
        res.json(ret);
        return;
    } catch (error) {
        ret.msg = "未知错误";
        res.json(ret);
        return;
    }
});

client.use((req, res) => {
    res.send("404")
})
module.exports = {client}